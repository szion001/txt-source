﻿
「............咦？」

回過神來，我已經站在森林裡，這陌生的環境讓我呆住了。
這是哪？我是誰？......現在不是開玩笑的時候啊。

「不，真的這是哪裡！？莎莉亞！阿爾！海倫！」

我情不自禁地喊了起來，但沒有回應。
雖然對這件事感到不安，但我想起了掛在自己脖子上的『無盡之愛的項鍊』的效果，於是立刻試著和莎莉亞她們聯繫。

「莎莉亞！能聽到嗎！？」
『――――啊、誠一！聽得見，我聽到了！』

從項鍊中傳來聲音讓我放心，我鬆了一口氣。
去冥界的時候幾乎都是按照自己的意志去的，所以沒什麼不安，但是一旦被迫飛到這種陌生的地方，膽小的我就會非常不安。
而且，這次和冥界的時候不同，因為項鍊的效果能取得聯絡，所以情況還算不壞。

『喂，誠一！你現在在哪裡！？』

我剛放下心來，這次又聽到了阿爾焦急的聲音。阿爾平時一直充當吐槽的角色，連這種時候都讓她擔心，我真的感到非常抱歉。

「就算你問我在哪裡......說實話，我也不知道。」

因為，眼前是一片鬱鬱蔥蔥的森林。
環視四周，到處都是樹木，老實說，無法判斷這是哪裡。
只是，項鍊還發揮效果的話，我應該還在這個星球的某個地方，想辦法應該可以和她們會合。
於是，這次傳來了莎莉亞輕快的聲音。

『太好了！雖然覺得誠一沒問題，但我還是很擔心......』
「莎莉亞......對不起。」

就連天真爛漫的莎莉亞都在擔心我，真是對不起她們。

『如果是誠一的話，用轉移魔法就能返還吧？快點回來吧。不單是為了把迪斯特拉那個混帳交給國王，重要的是......我想早點見到你』

腦海中浮現出一邊紅著臉一邊這麼說的阿爾的身影，我確實想起了用轉移魔法馬上就能回去的事情，於是我立刻發動魔法。
但是――――。

「咦？」
『誠一，怎麼了？』
「不，只是......。」

雖然好幾次想發動轉移魔法，但不知為何轉移魔法卻一直沒有發動。

「為什麼？是因為這個地方嗎？」

即使那樣，假如是因為這個地方才用不了轉移魔法的話，我那不自重的身體應該不會忽略的......。
今天大活躍的腦內廣播小姐對困惑的我說。

『誠一大人。這個地方好像是不能使用魔法的』
「誒？那不能像往常一樣發動【進化】來使用魔法嗎？」
『很遺憾......。在這片土地上無法使用魔法，並不是因為這片特殊的土地干涉了誠一大人，而是干涉著魔法本身，因此【進化】的效果沒有發動』

還有這種事。
來到這裡，我第一次發現【進化】的弱點，或者說是這個技能的漏洞。
對我有效果或干涉我的東西無一例外適應掉，不過，如果對象不是我的身體，【進化】的效果就發揮不了。
如果說因為這個我的怪物性有所變薄，老實說這也就「哪有又怎樣？」的程度，什麼都沒改變哦......只是在這種時候顯得有點不方便。
不管怎麼說，只要和我有關係的話就會發動【進化】，結果被稱為作弊或者bug這一點沒有改變。儘管如此，我對【進化】不是完美的事稍微有點放心。
總之，得知現在不能馬上回去，我歎了一口氣後，通過項鍊向莎莉亞她們傳達這件事。

「這個地方好像無法使用魔法，總之我先移動到可以使用魔法的地方，然後再回去吧。」
『......沒問題吧？』

於是從項鍊中傳來阿爾擔心的聲音，我不禁苦笑起來。

「嗯......雖然不能斷言沒問題，但我一定會回來的。而且，現在還能與你們通信，有什麼事會馬上聯繫妳們的。」
『......嗯。我們等著你回來』
「啊，你那邊有什麼事的話，要馬上聯繫我哦？就算是──把這片森林吹飛我也會回去的。」

如果是這個森林阻礙魔法發動的話，作為最終手段，把森林抹掉就沒有問題了。我想到這種笨蛋一樣的方法，看來我的思考也慢慢地受身體擺佈了......話說回來，明明是笨蛋一樣的想法，但好像能做到這讓我很無語......。
我不想破壞這裡的環境，但是如果莎莉亞她們發生了什麼事，我甚至會無視這些東西。

「那麼，如果有什麼進展的話再聯絡吧。」
『嗯，小心點！』

聽了莎莉亞的話，我們暫時結束了通信。

「那麼......不知道這裡是哪裡，只好隨便走啦。」

雖說在山裡亂跑很危險，但是最初來到異世界也是在超危險的森林，現在應該能比那個時候更安全，總會有辦法的。

「也就是說......來試試運氣吧。」

我適當地撿起掉在地上的樹枝，立在地上。

「那麼，會倒向哪個方向呢？」

我馬上放開樹枝，它向右倒下。

「好的，右邊。」

現在狀態正離家出走，雖然不知道準確的數值，但是運氣應該不錯，大概沒問題吧......不，仔細想想在它離家出走之前也沒有顯示出來，實際是怎麼回事呢？
總之，我開始在這莫名其妙的森林中遊蕩。

◆◇◆

「可惡，誠一那傢伙......為什麼那傢伙總被捲入麻煩中呢！？」

──誠一突然消失後，留在迷宮的莎莉亞她們和誠一取得了聯繫。
並且，確認誠一在與迷宮完全不同的地方平安無事，她們都鬆了口氣。
但是，平時的話應該用轉移魔法馬上能返回，不過，聽誠一說被送到的地方不能使用魔法，暫時只能在周圍探索。

「難道是......我，是因為我的詛咒嗎！？明明以為已經沒問題了......！」

突然，一直因為詛咒而受苦的阿爾臉色變得蒼白，她認為是自己的詛咒讓誠一遭受了災難的。

「誠一的話一定沒問題的！而且，這不是阿爾的錯喲？」
「但、但是......。」

莎莉亞溫柔地擁抱著臉上還帶著不安的表情的阿爾。

「沒關係。誠一說他會回來的，就算真的是阿爾的詛咒落到誠一身上，他也會馬上從詛咒中逃出來的！」
「......那是，什麼......但是，真奇怪啊我不能否定這個說法......。」

被莎莉亞抱著，恢復冷靜的阿爾露出苦笑。

「謝謝妳，莎莉亞。」
「嗯！」

恢復原樣的阿爾將目光轉向了至今仍然昏迷的迪斯特拉。

「那麼......因為誠一把他所有的東西都回收了，我覺得這傢伙已經沒有危險了......。」
「啊，我想起來了！誠一掉下來的水晶，不是以前在學園士兵們和誠一戰鬥的時候，最後使用的東西嗎？」
「啊，那時候連戰鬥都說不上！雖然不知道詳細的效果，但是好像可以轉移到任何地方，現在誠一所在的地方，說不定就是迪斯特拉那傢伙接下來要去的地方。」
「總之我們先把這個人交給這個國家的士兵吧！」
「是啊。」

阿爾把迪斯特拉胡亂地抬起來，突然她發現海倫的樣子很奇怪。

「......。」
「啊？喂，怎麼了？」
「......魔法......不能使用的地方......？不，但是......。」
「海倫醬？」
「啊！怎、怎麼了？」

莎莉亞窺視著海倫的臉，海倫終於注意到了莎莉亞和阿爾。

「什麼怎麼了......妳的樣子很奇怪啊。」
「......我有點在意的事。不過，如果那傢伙是【魔神教團】的幹部的話，也有可能轉移到基地呢......。」
「嗯？說得對......也有轉移到基地的情況嗎......。」

雖然阿爾對出現新的可能性皺起了眉頭，但她馬上搖了搖頭。

「不管怎麼想都沒辦法。總之，先把這傢伙從迷宮裡帶出來。可以嗎？」
「是的，沒關係。提高等級的目標也完成了......。」
「好，那就回去吧。」

就這樣在誠一開始探索森林的時候，莎莉亞她們回到了王都。


