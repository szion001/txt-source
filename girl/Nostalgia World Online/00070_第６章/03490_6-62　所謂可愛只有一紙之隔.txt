「誒—哆，話說回來，好像差不多該到了吧…」
根據委託的內容，從湖之都市向南移動，在河流變寬的地區附近似乎會出現河畔水獺。

而我們現在就在附近…。

「沒有發現類似的大型怪物啊…」
「剛試著從天空上看，不過，都只看到小動物型的怪物～。之後就只有岩石等散佈著而已～」
「呼呣…不過這個是從代表那拿到的委託，情報不會有錯誤才對…」
「嗯—…說不定是在河裡面也有可能呢！」
那麼作為先發制人，試著往河裡打上一發會比較好嗎？

是否要對河川進行砲擊呢？大家正在議論著時，突然發生巨大的搖晃，我們立刻對周圍進行警戒。

但是，周圍什麼會動的生物的身影都沒有，作為代替的是有什麼東西在往我們接近。

「欸…？」
「巨大的岩石…」
「竟然…在動…！？」
慢慢吞吞地向我們這邊接近的是巨大的茶色岩石。

之後它停止了移動，隨即從中心產生裂縫，裂縫向全體擴散後碎裂，然後從裡面出來的是…。

「啾嗚嗚嗚嗚嗚嗚嗚！」
是一個有４米高的巨大水獺。

「「「「啊，好可愛…」」」」
包括我在內的女性陣容，在凝視著巨大水獺後都不知不覺這樣嘟噥了。

這不是沒辦法嘛！

因為好可愛蒙！

「嗚—呣……是要我們退治這個嗎……」
「你在說什麼啊？！這麼無害又可愛的存在，我怎麼可能狠心下去退治呢！」
「對啊對啊！」
「不……但是……」
對於團長的嘟噥，銀翼的其他女性團員發出了抗議的聲音。

不愧就算是團長先生，也有點被女性成員的氣勢壓倒了。

另一方面，河畔水獺一邊看著我們一邊歪著頭，然後一邊向周圍張望地觀察情況。

對於那個動作，女性陣容高聲尖叫，更加熱鬧。

「啾嗚嗚嗚…鯊啊啊啊啊啊啊啊！」
「？！」
但是下一個瞬間，露出可愛表情的河畔水獺突然發出了威嚇我們的叫聲，全身的毛髮開始倒立起來。

「全員！進入戰鬥態勢！快點！」
團長先生的呼叫聲，讓由於突發狀況被嚇得目瞪口呆的我們慌張張地進入戰鬥態勢。

但是，河畔水獺像嘲笑我們的陣形般，用巨大的尾巴劈了下來。

「！？全員回避！」
「什！？」
伊庫莉露小姐剎那間發出的指示，大家立即住旁邊飛撲，總算是躲避了河畔水獺的攻擊。

實際上伊庫莉露小姐的判斷是正確的。

被甩下來的尾巴砸碎了地面，是有讓周圍的地面也出現裂縫的威力。

「列維的尾巴，也不會有那種威力呢……」
「恐怕河畔水獺的尾巴是有著出奇的重量，或許牠是想要炫耀著自己尾巴的硬度嗎？我覺得，至少也要警戒一下那個尾巴呢？」
「而且是在岩石中出現的關係，是土屬性和水屬性的意思吧？是屬於很難理解弱點的屬性呢……」
「搞不好可能是鋼鐵吧。而且從河畔來看，火和斬擊系可能都不好使」
「……那麼就是說，我什麼也做不到不是嗎？」
「嘛……如果是大小姐的話，總會想到辦法的吧？」
真是的！杔椏小姐在隨便說什麼啊！

妳以為我是什麼啊！

喋，河畔水獺的尾巴開始轉起來了，到底……。

「坦克馬上展開盾牌！有什麼要飛過來了！」
「什！？『土之盾』！」
我聽從伊庫莉露小姐的指示，慌忙展開幾重的土牆壁。

下一個瞬間，從河畔水獺的尾巴上不知有什麼，像黑針一樣的細小的東西射向我們。

「咕！『延遲』！」
「『土之盾』！」
一瞬間前排的土壁被貫通，然後在再穿過幾層就快要到達我們之前，因為杔椏使用的『延遲』而減慢了土牆被破壞的速度。

我配合著這個的瞬間，再施展開幾重的土牆後，總算是想辦法的防禦下來了。

「伊庫莉露小姐！」
「我們這邊全員平安！愛麗絲小姐妳們那邊情況呢！」
「多虧了杔椏小姐，總算逃過一劫！」
即便如此，真沒想到這攻擊的一發一發竟然如此高的貫徹威力。

到這裡為止，感覺這怪物的水準一口氣上升了。

「大小姐，請看這個。」
杔椏小姐拿起河畔水獺放飛的針給我看。

「這個針…不，請看這體毛的硬度。就像鋼的硬度般堅硬。」
「這完全不是河畔吧。說是礦山會比較符合不是嗎？」
「是把河川內的礦物吃下後，使體毛發生變化了嗎？」
「看來，這果然是……」
「是的，大小姐的攻擊很難起作用呢？」
當真怎麼辦？…。

「鯊鯊啊啊啊啊啊啊！」
河畔水獺突然發狂般的開始咆哮。
或許是對我們避開或阻擋了其攻擊，覺得很看不慣的意思嗎……？

「完全生氣起來了呢。從最初就開始激昂模式什麼的，請饒了我吧。」
「現在不是說這個的場、合……要來了！」
河畔水獺以猛烈的氣勢向銀翼的坦克們突進。

「前衛！鼓起幹勁了喔！『城壁』！」
團長先生們的坦克軍團使用了技能後，便停止河畔水獺的突進。

然後，趁著目標停止的這個機會，後衛的魔法軍團便用魔法進行攻擊。

「鏘？！」
「好硬哇呢～」
「但是，集中於一點重點攻擊的話，總會有辦法的呢。」
河畔水獺受到凜他們的攻擊後向後仰，不過，馬上左右搖了搖頭，再次瞪向這邊。

「從這樣子來看，用高火力的魔法來削弱牠是最好的吧？」
「也就是說，我們是要做支援役嗎？」
哈喋撒喋，看來會成為很辛苦的戰鬥呢。
