# HISTORY

## 2019-12-14

### Epub

- [靠廢柴技能【狀態異常】成為最強的我將蹂躪一切](dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87) - dmzj
  <br/>( v: 2 , c: 25, add: 12 )
- [處刑少女的生存之道](girl_out/%E8%99%95%E5%88%91%E5%B0%91%E5%A5%B3%E7%9A%84%E7%94%9F%E5%AD%98%E4%B9%8B%E9%81%93) - girl_out
  <br/>( v: 1 , c: 10, add: 10 )

### Segment

- [靠廢柴技能【狀態異常】成為最強的我將蹂躪一切](dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87) - dmzj
  <br/>( s: 1 )

## 2019-12-12

### Epub

- [漆黑使的最強勇者](syosetu_out/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85) - syosetu_out
  <br/>( v: 2 , c: 22, add: 0 )
- [異世界迷宮都市治癒魔法師](wenku8_out/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BF%B7%E5%AE%AE%E9%83%BD%E5%B8%82%E6%B2%BB%E7%99%92%E9%AD%94%E6%B3%95%E5%B8%AB) - wenku8_out
  <br/>( v: 4 , c: 57, add: 57 )

## 2019-12-10

### Epub

- [漆黑使的最強勇者](syosetu_out/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85) - syosetu_out
  <br/>( v: 2 , c: 22, add: 22 )

## 2019-12-04

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 96, add: 0 )

## 2019-12-01

### Epub

- [梗角色轉生太過頭了！](ts/%E6%A2%97%E8%A7%92%E8%89%B2%E8%BD%89%E7%94%9F%E5%A4%AA%E9%81%8E%E9%A0%AD%E4%BA%86%EF%BC%81) - ts
  <br/>( v: 1 , c: 37, add: 37 )
- [２９歳独身は異世界で自由に生きた…かった。](user_out/%EF%BC%92%EF%BC%99%E6%AD%B3%E7%8B%AC%E8%BA%AB%E3%81%AF%E7%95%B0%E4%B8%96%E7%95%8C%E3%81%A7%E8%87%AA%E7%94%B1%E3%81%AB%E7%94%9F%E3%81%8D%E3%81%9F%E2%80%A6%E3%81%8B%E3%81%A3%E3%81%9F%E3%80%82) - user_out
  <br/>( v: 15 , c: 141, add: 10 )

### Segment

- [２９歳独身は異世界で自由に生きた…かった。](user/%EF%BC%92%EF%BC%99%E6%AD%B3%E7%8B%AC%E8%BA%AB%E3%81%AF%E7%95%B0%E4%B8%96%E7%95%8C%E3%81%A7%E8%87%AA%E7%94%B1%E3%81%AB%E7%94%9F%E3%81%8D%E3%81%9F%E2%80%A6%E3%81%8B%E3%81%A3%E3%81%9F%E3%80%82) - user
  <br/>( s: 2 )

## 2019-11-30

### Epub

- [世界最強魔王潛入勇者育成機關](school/%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7%E9%AD%94%E7%8E%8B%E6%BD%9B%E5%85%A5%E5%8B%87%E8%80%85%E8%82%B2%E6%88%90%E6%A9%9F%E9%97%9C) - school
  <br/>( v: 1 , c: 49, add: 49 )
- [平凡職業造就世界最強](wenku8_out/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8_out
  <br/>( v: 30 , c: 498, add: 18 )

### Segment

- [平凡職業造就世界最強](wenku8/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8
  <br/>( s: 12 )

## 2019-11-29

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 96, add: 1 )
- [転生ごときで逃げられるとでも、兄さん？](yandere_out/%E8%BB%A2%E7%94%9F%E3%81%94%E3%81%A8%E3%81%8D%E3%81%A7%E9%80%83%E3%81%92%E3%82%89%E3%82%8C%E3%82%8B%E3%81%A8%E3%81%A7%E3%82%82%E3%80%81%E5%85%84%E3%81%95%E3%82%93%EF%BC%9F) - yandere_out
  <br/>( v: 6 , c: 164, add: 11 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-11-28

### Epub

- [四度目は嫌な死属性魔術師](user_out/%E5%9B%9B%E5%BA%A6%E7%9B%AE%E3%81%AF%E5%AB%8C%E3%81%AA%E6%AD%BB%E5%B1%9E%E6%80%A7%E9%AD%94%E8%A1%93%E5%B8%AB) - user_out
  <br/>( v: 18 , c: 282, add: 0 )

## 2019-11-27

### Epub

- [魔王軍最強の魔術師は人間だった](syosetu_out/%E9%AD%94%E7%8E%8B%E8%BB%8D%E6%9C%80%E5%BC%B7%E3%81%AE%E9%AD%94%E8%A1%93%E5%B8%AB%E3%81%AF%E4%BA%BA%E9%96%93%E3%81%A0%E3%81%A3%E3%81%9F) - syosetu_out
  <br/>( v: 1 , c: 31, add: 0 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 95, add: 2 )
- [俺の死亡フラグが留まるところを知らない](user_out/%E4%BF%BA%E3%81%AE%E6%AD%BB%E4%BA%A1%E3%83%95%E3%83%A9%E3%82%B0%E3%81%8C%E7%95%99%E3%81%BE%E3%82%8B%E3%81%A8%E3%81%93%E3%82%8D%E3%82%92%E7%9F%A5%E3%82%89%E3%81%AA%E3%81%84) - user_out
  <br/>( v: 5 , c: 110, add: 3 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-11-24

### Epub

- [新世界のメイド（仮）さんと女神様](ts_out/%E6%96%B0%E4%B8%96%E7%95%8C%E3%81%AE%E3%83%A1%E3%82%A4%E3%83%89%EF%BC%88%E4%BB%AE%EF%BC%89%E3%81%95%E3%82%93%E3%81%A8%E5%A5%B3%E7%A5%9E%E6%A7%98) - ts_out
  <br/>( v: 5 , c: 127, add: 0 )

## 2019-11-23

### Epub

- [新世界のメイド（仮）さんと女神様](ts_out/%E6%96%B0%E4%B8%96%E7%95%8C%E3%81%AE%E3%83%A1%E3%82%A4%E3%83%89%EF%BC%88%E4%BB%AE%EF%BC%89%E3%81%95%E3%82%93%E3%81%A8%E5%A5%B3%E7%A5%9E%E6%A7%98) - ts_out
  <br/>( v: 5 , c: 127, add: 127 )

### Segment

- [新世界のメイド（仮）さんと女神様](ts/%E6%96%B0%E4%B8%96%E7%95%8C%E3%81%AE%E3%83%A1%E3%82%A4%E3%83%89%EF%BC%88%E4%BB%AE%EF%BC%89%E3%81%95%E3%82%93%E3%81%A8%E5%A5%B3%E7%A5%9E%E6%A7%98) - ts
  <br/>( s: 7 )



